{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Lib
import           Network.HTTP.Types
import           Network.Wai
import           Network.Wai.Handler.Warp (run)
import           System.Environment


app :: Application
app _ respond = do
    putStrLn "I've done some IO here"
    respond $ responseLBS
        status200
        [("Content-Type", "text/plain")]
        "Hello from my Haskell backend! -Mikhail Baynov"

main :: IO ()
main = do
  port <- read <$> getEnv "PORT"
  run port app
