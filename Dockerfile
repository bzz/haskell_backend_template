FROM haskell:7.10
RUN cabal update
ADD ./backend.cabal /opt/server/backend.cabal 
RUN cd /opt/server && cabal install --only-dependencies -j4
ADD ./src /opt/server/src
ADD ./exe /opt/server/exe
RUN cd /opt/server && cabal install
ENV PATH /root/.cabal/bin:$PATH
ENV PORT 80
WORKDIR /opt/server/
CMD ["backend"]


## CREATE DOCKER CONTAINER:
## docker build . -t backend
## docker run -p 80:80 -e PORT=80 backend


## HEROKU PUSH CONTAINER:
## heroku container:login
## (to create an app) heroku create
## heroku container:push web --app mbaynov
## heroku open --app mbaynov
## heroku container:release web --app mbaynov

## DOCKER PUSH CONTAINER (alternative)
## heroku auth:token
## docker login --username=_ --password=2fe5c501-babe-401d-b97d-5a91f48478c8 registry.heroku.com
## docker build -t registry.heroku.com/mbaynov/web .
## docker push registry.heroku.com/mbaynov/web

## heroku logs --app mbaynov